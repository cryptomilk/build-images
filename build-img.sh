#!/bin/sh

image=${1}
startdir=${2}
registry=${3}
project=${4}

echo "ref: ${CI_COMMIT_REF_NAME}"
echo "namespace: ${CI_PROJECT_NAMESPACE}"
echo "project: ${CI_PROJECT_NAME}"

PODMAN=$(command -v podman)
if test -z "${PODMAN}"; then
    echo "Could not detect podman"
    exit 1
fi

main_build=0
if test "${CI_PROJECT_NAMESPACE}" = "cryptomilk" &&
   test "${CI_COMMIT_REF_NAME}" = "main" &&
   test "${CI_PROJECT_NAME}" = "build-images"; then
    main_build=1
fi

set -e
${PODMAN} login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${registry}"
${PODMAN} build --no-cache -t "${registry}/${project}:${image}" "${startdir}"

if test ${main_build} -eq 0; then
    echo "Not a master build"
    exit 0
else
    ${PODMAN} push "${registry}/${project}:${image}"
fi

${PODMAN} logout "${registry}"

exit 0
